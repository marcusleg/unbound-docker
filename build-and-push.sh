#!/bin/bash
parallel --tag podman build -f Dockerfile.{} -t docker.io/orangefoil/unbound:{}-latest . ::: amd64 arm32v7 arm64v8
parallel --tag podman push docker.io/orangefoil/unbound:{}-latest ::: amd64 arm32v7 arm64v8

podman tag orangefoil/unbound:amd64-latest docker.io/orangefoil/unbound:latest
podman push docker.io/orangefoil/unbound:latest
