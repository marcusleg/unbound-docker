# Unbound Docker

Run your own recursive DNS resolver and protect your privacy.

## How to use this image

`docker run -p 53:53 docker pull docker.io/orangefoil/unbound`
